/*
 * Author and owner: Adam Nok [nokadams@gmail.com] https://www.linkedin.com/in/nokadams
 */
package org.dongoteam.dfaexample

import org.dongoteam.pendler.ego.items.Clickable
import org.dongoteam.pendler.ego.items.Hoverable
import org.dongoteam.pendler.ego.items.Typeable
import org.dongoteam.pendler.envs.dfa.DFAEngine
import org.dongoteam.pendler.envs.dfa.DFATest
import org.dongoteam.pendler.envs.dfa.console.ConsoleEnvironment

fun main() {
    DFAEngine(
        mapOf(
            "url" to "http://localhost:8000"
        ),
        ConsoleEnvironment()
    ).start<WithoutConfig2DFATest>(false)
}

/** metadesc
 * @Name Példateszt
 * @Meta TechnicalWriter Nok Ádám @nokadams
 * @Meta Environments [BSA, BGD]
 * @Meta FeatureDirectory /standard/wh-config
 *
 * # Címsor példa 1
 *
 * - Egy példa a működés tesztelésére.
 */
class WithoutConfig2DFATest : DFATest({


    /** metadoc
     *
     * @Name Inicializálás
     *
     * Tevékenységek:
     * - Böngésző megnyitása
     */
    task {
        web {
            open(global["url"] as String) alias "default"
        }
    }

    /** metadoc
     *
     * @Name Alapfunkciók tesztelése
     *
     * Adjuk hozzá a *We have to make a dinner.* feladatot a TODO listához, majd a
     * *We should learn cooking.* feladatot is.
     *
     * Ellenőrizzük le, hogy kívánt feladatok hozzáadódtak a listához az előírt sorrendnek
     * megfelelően (leutoljára felvett van alul), majd töröljük ki őket.
     *
     * A törlés után a TODO listának üresnek kell lennie.
     */
    task {
        web {
            typing("*We have to make a dinner.*\n") on xpath("/html/body/section/header/input")
            typing("*We should learn cooking.*\n") on xpath("/html/body/section/header/input")

            expected("*We have to make a dinner.*") equals xpath("/html/body/section/section/ul/li[1]/div/label")
            expected("*We should learn cooking.*") equals xpath("/html/body/section/section/ul/li[2]/div/label")
        }
        code {
            page.find<Hoverable>(xpath("/html/body/section/section/ul/li[1]")).hover()
            page.find<Clickable>(xpath("/html/body/section/section/ul/li[1]/div/button")).click()

            page<Hoverable>(xpath("/html/body/section/section/ul/li[1]")).hover()
            page<Clickable>(xpath("/html/body/section/section/ul/li[1]/div/button")).click()
        }
    }

    /** metadoc
     * @Name TODO feladatok szűrése
     *
     * Ismét vegyük fel a *We have to make a dinner.* és a *We should learn cooking.* felatokat
     * a TODO listára, majd a *We should learn cooking.* feladatot jelöljük ki.
     */
    task {
        /** metadoc
         *
         * @Name Tesztadat létrehozása
         */
        task {
            web {
                typing("*We have to make a dinner.*\n") on xpath("/html/body/section/header/input")
                typing("*We should learn cooking.*\n") on xpath("/html/body/section/header/input")
                click on xpath("/html/body/section/section/ul/li[2]/div/input")
            }
        }

        /** metadoc
         *
         * @Name **Active** szűrőmező
         *
         * Válasszuk ki az **Active** feliratú szűrőmezőt és ellenőrizzük, hogy valóban csak a
         * *We have to make a dinner.* feladat jelenik-e meg!
         */
        task {
            web {
                click on xpath("/html/body/section/footer/ul/li[2]/a")
                expected("*We have to make a dinner.*") equals xpath("/html/body/section/section/ul/li[1]/div/label")
            }
            code {
                assertTrue(page.exists(xpath("/html/body/section/section/ul/li[1]")))
                assertFalse(page.exists(xpath("/html/body/section/section/ul/li[2]")))
            }
        }

        /** metadoc
         *
         * @Name **Completed** szűrőmező
         *
         * Válasszuk ki az **Completed** feliratú szűrőmezőt és ellenőrizzük, hogy valóban csak a
         * *We should learn cooking.* feladat jelenik-e meg!
         */
        task {
            web {
                click on xpath("/html/body/section/footer/ul/li[3]/a")
                expected("*We should learn cooking.*") equals xpath("/html/body/section/section/ul/li[1]/div/label")
            }
            code {
                assertTrue(page.exists(xpath("/html/body/section/section/ul/li[1]")))
                assertFalse(page.exists(xpath("/html/body/section/section/ul/li[2]")))
            }
        }

        /** metadoc
         *
         * @Name **All** szűrőmező
         *
         * Végezetül kattintsunk az **All** feliratú szűrőmezőre és ellenőrizzük le, hogy minden
         * felvett feladat megjelenik-e:
         *  - *We have to make a dinner.*
         *  - *We should learn cooking.*
         */
        task {
            web {
                click on xpath("/html/body/section/footer/ul/li[1]/a")
                expected("*We have to make a dinner.*") equals xpath("/html/body/section/section/ul/li[1]/div/label")
                expected("*We should learn cooking.*") equals xpath("/html/body/section/section/ul/li[2]/div/label")
            }
        }
    }

    /** metadoc
     *
     * @Name Feladatmódosítás
     *
     * Egy már felvett feladat tartalmának módosítása.
     */
    task {
        code {
            page<Clickable>(xpath("/html/body/section/section/ul/li[1]/div/label")).doubleClick()
            page<Typeable>(xpath("/html/body/section/section/ul/li[1]/input"))
                .clear()
                .typing("Bamba")
        }
    }

})