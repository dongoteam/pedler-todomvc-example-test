/*
 * Author and owner: Adam Nok [nokadams@gmail.com] https://www.linkedin.com/in/nokadams
 */
package org.dongoteam.dfaexample.config

import org.dongoteam.pendler.envs.dfa.DFAConfig
import org.dongoteam.pendler.envs.dfa.Global

class  TodoMVCConfig : DFAConfig() {


    override fun initTestCase(global: Global): Task =
        Task {
            web {
                open(global["url"] as String) alias "default"
            }
            code {
            }
        }

    override fun endTestCase(global: Global): Task =
        Task {
            web {
                close()
            }
            code {

            }
        }

    override fun onErrorTestCase(global: Global): Task =
        Task {
            code {

            }
        }

}