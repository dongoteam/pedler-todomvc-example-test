/*
 * Author and owner: Adam Nok [nokadams@gmail.com] https://www.linkedin.com/in/nokadams
 */
package org.dongoteam.dfaexample

import org.dongoteam.pendler.ego.items.Clickable
import org.dongoteam.pendler.ego.items.Getter
import org.dongoteam.pendler.ego.items.Hoverable
import org.dongoteam.pendler.ego.items.Typeable
import org.dongoteam.pendler.envs.dfa.DFAConfig
import org.dongoteam.pendler.envs.dfa.DFAEngine
import org.dongoteam.pendler.envs.dfa.DFATest
import org.dongoteam.pendler.envs.dfa.Global
import org.dongoteam.pendler.envs.dfa.console.ConsoleEnvironment

fun main() {
    DFAEngine(
        mapOf(
            "url" to "http://localhost:8000"
        ),
        ConsoleEnvironment()
    ).start<WithoutConfigDFATest>(false)
}

/** metadesc
 * @Name Példateszt
 * @Meta TechnicalWriter Nok Ádám @nokadams
 * @Meta Environments [BSA, BGD]
 * @Meta FeatureDirectory /standard/wh-config
 *
 * # Címsor példa 1
 *
 * - Egy példa a működés tesztelésére.
 */
class WithoutConfigDFATest : DFATest({

    /** metadoc
     *
     * @Name Inicializálás
     */
    taskCode {
        createAndAssignPage(global["url"] as String)
    }

    /** metadoc
     *
     * @Name Alapfunkciók tesztelése
     *
     * Adjuk hozzá a *We have to make a dinner.* feladatot a TODO listához, majd a
     * *We should learn cooking.* feladatot is.
     *
     * Ellenőrizzük le, hogy kívánt feladatok hozzáadódtak a listához az előírt sorrendnek
     * megfelelően (leutoljára felvett van alul), majd töröljük ki őket.
     *
     * A törlés után a TODO listának üresnek kell lennie.
     */
    taskCode {
        page<Typeable>(xpath("/html/body/section/header/input"))
            .typing("*We have to make a dinner.*\n")

        page<Typeable>(xpath("/html/body/section/header/input"))
            .typing("*We should learn cooking.*\n")

        assertEquals(
            "*We have to make a dinner.*",
            page<Getter>(xpath("/html/body/section/section/ul/li[1]/div/label"))
                .text()
        )
        assertEquals(
            "*We should learn cooking.*",
            page<Getter>(xpath("/html/body/section/section/ul/li[2]/div/label"))
                .text()
        )
        page.find<Hoverable>(xpath("/html/body/section/section/ul/li[1]"))
            .hover()
        page.find<Clickable>(xpath("/html/body/section/section/ul/li[1]/div/button"))
            .click()

        page<Hoverable>(xpath("/html/body/section/section/ul/li[1]"))
            .hover()
        page<Clickable>(xpath("/html/body/section/section/ul/li[1]/div/button"))
            .click()
    }

    /** metadoc
     * @Name TODO feladatok szűrése
     *
     * Ismét vegyük fel a *We have to make a dinner.* és a *We should learn cooking.* felatokat
     * a TODO listára, majd a *We should learn cooking.* feladatot jelöljük ki.
     */
    task {
        /** metadoc
         *
         * @Name Tesztadat létrehozása
         */
        taskCode {
            page<Typeable>(xpath("/html/body/section/header/input"))
                .typing("*We have to make a dinner.*\n")
            page<Typeable>(xpath("/html/body/section/header/input"))
                .typing("*We should learn cooking.*\n")
            page<Clickable>(xpath("/html/body/section/section/ul/li[2]/div/input"))
                .click()
        }

        /** metadoc
         *
         * @Name **Active** szűrőmező
         *
         * Válasszuk ki az **Active** feliratú szűrőmezőt és ellenőrizzük, hogy valóban csak a
         * *We have to make a dinner.* feladat jelenik-e meg!
         */
        taskCode {
            page<Clickable>(xpath("/html/body/section/footer/ul/li[2]/a"))
                .click()
            assertEquals(
                "*We have to make a dinner.*",
                page<Getter>(xpath("/html/body/section/section/ul/li[1]/div/label"))
                    .text()
            )
            assertTrue(page.exists(xpath("/html/body/section/section/ul/li[1]")))
            assertFalse(page.exists(xpath("/html/body/section/section/ul/li[2]")))
        }

        /** metadoc
         *
         * @Name **Completed** szűrőmező
         *
         * Válasszuk ki az **Completed** feliratú szűrőmezőt és ellenőrizzük, hogy valóban csak a
         * *We should learn cooking.* feladat jelenik-e meg!
         */
        taskCode {
            page<Clickable>(xpath("/html/body/section/footer/ul/li[3]/a"))
                .click()
            assertEquals(
                "*We should learn cooking.*",
                page<Getter>(xpath("/html/body/section/section/ul/li[1]/div/label"))
                    .text()
            )
            assertTrue(page.exists(xpath("/html/body/section/section/ul/li[1]")))
            assertFalse(page.exists(xpath("/html/body/section/section/ul/li[2]")))
        }

        /** metadoc
         *
         * @Name **All** szűrőmező
         *
         * Végezetül kattintsunk az **All** feliratú szűrőmezőre és ellenőrizzük le, hogy minden
         * felvett feladat megjelenik-e:
         *  - *We have to make a dinner.*
         *  - *We should learn cooking.*
         */
        taskCode {
            page<Clickable>(xpath("/html/body/section/footer/ul/li[1]/a"))
                .click()
            assertEquals(
                "*We have to make a dinner.*",
                page<Getter>(xpath("/html/body/section/section/ul/li[1]/div/label"))
                    .text()
            )
            assertEquals(
                "*We should learn cooking.*",
                page<Getter>(xpath("/html/body/section/section/ul/li[2]/div/label"))
                    .text()
            )
        }
    }

    /** metadoc
     *
     * @Name Feladatmódosítás
     *
     * Egy már felvett feladat tartalmának módosítása.
     */
    taskCode {
        page<Clickable>(xpath("/html/body/section/section/ul/li[1]/div/label")).doubleClick()
        page<Typeable>(xpath("/html/body/section/section/ul/li[1]/input"))
            .clear()
            .typing("Bamba")
    }

})